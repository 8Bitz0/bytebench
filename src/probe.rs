use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct ThreadInfo {
    pub id: usize,
    pub run_time: u128,
    pub score: usize
}
