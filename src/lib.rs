// warnings: "mass-change the level for lints which produce warnings"
#![deny(warnings)]
// dead_code: "detect unused, unexported items"
#![allow(dead_code)]
// unreachable_code: "detects unreachable code paths"
#![allow(unreachable_code)]

pub mod bench;
pub mod config;
pub mod export;
pub mod probe;
pub mod scores;
pub mod system;
pub mod tests;
pub mod ui;
pub mod view;

