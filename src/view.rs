use crate::export;

pub fn print_info(results: export::DataExportFormat) {
    println!("Processor Single-Core:        {}", results.scores.processor_scores[0]);
    println!("Processor Multi-Core:         {}\n", results.scores.processor_scores[1]);

    println!("Reverse String Single-Core:   {}", results.scores.reverse_string_scores[0]);
    println!("Reverse String Multi-Core:    {}\n", results.scores.reverse_string_scores[1]);

    println!("Compression Single-Core:      {}", results.scores.compression_scores[0]);
    println!("Compression Multi-Core:       {}", results.scores.compression_scores[1]);
}
