// warnings: "mass-change the level for lints which produce warnings"
#![deny(warnings)]
// dead_code: "detect unused, unexported items"
#![allow(dead_code)]
// unreachable_code: "detects unreachable code paths"
#![allow(unreachable_code)]

pub struct Test {
    pub name: String,
    pub id: String,
}

pub mod reverse;
pub mod compress;

pub fn get_tests() -> Vec<Test> {
    return vec![
        Test {
            name: String::from("Reverse String"),
            id: String::from("reverse_string")
        },
        Test {
            name: String::from("Compression"),
            id: String::from("compression")
        }
    ]
}
