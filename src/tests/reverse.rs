use log::{debug, error};

use synchronoise::SignalEvent;

use std::str::from_utf8;
use std::sync::Arc;

use crate::probe::{self, ThreadInfo};

fn reverse(text: &str) -> String {
    text.chars().rev().collect()
}

pub fn start(cores: usize) -> Result<Vec<ThreadInfo>, Box<dyn std::error::Error>> {
    let mut results: Vec<ThreadInfo> = vec![];

    let (sender, receiver) = std::sync::mpsc::channel();

    let start_signal = Arc::new(SignalEvent::manual(false));

    for i in 0..cores {
        let sender_n = sender.clone();

        let thread_num = i.clone();

        let start  = start_signal.clone();

        std::thread::spawn(move || {
            debug!("Thread {} spawn", &thread_num);

            let mut score: usize = 0;

            // This creates a pretty large string
            let text = match from_utf8(include_bytes!("lorem_ipsum.txt")) {
                Ok(o) => o,
                Err(_) => {
                    error!("Unable to decode sample text. Dropping score, results will be invalid! (Thread {})", thread_num);

                    panic!("Unable to decode sample text");
                }
            };
            let text = &text.repeat(1000);

            debug!("Prepared! Waiting for signal... (Thread {})", &thread_num);

            start.wait();

            debug!("Got signal! Test is starting... (Thread {})", &thread_num);

            let start_time = std::time::Instant::now();

            while start_time.elapsed() < std::time::Duration::from_secs(5) {
                let _ = reverse(text);

                if start_time.elapsed() < std::time::Duration::from_secs(5) {
                    score += 1;
                }
            }

            let run_time = start_time.elapsed();

            let thread_info = probe::ThreadInfo {
                id: thread_num,
                run_time: std::time::Duration::as_millis(&run_time),
                score: score
            };

            match sender_n.send(thread_info) {
                Ok(_) => {}
                Err(_) => {
                    error!("Main thread stopped listening for scores! Dropping score, results will be invalid! (Thread {})", thread_num);
                }
            }
        });
    }

    debug!("Threads spawned! Giving additional time for preparation...");

    std::thread::sleep(std::time::Duration::from_millis(500));

    debug!("Starting all threads!");

    start_signal.signal();
    
    for _ in 0..cores {
        match receiver.recv() {
            Ok(o) => {
                debug!("Got results from thread {:?}", o);
                results.push(o);
            }
            Err(e) => {
                error!("Thread failed, results not captured.");
                return Err(Box::new(e))
            }
        }
    }

    return Ok(results);
}

#[cfg(test)]
#[test]
fn test_reverse_string() {
    let input_string: &str = "Hello, World!";
    let expected_output: String = String::from("!dlroW ,olleH");

    assert_eq!(reverse(input_string), expected_output);
}
