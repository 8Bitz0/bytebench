use crate::bench;

use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
pub struct Scores {
    pub processor_scores: [usize; 2],

    pub reverse_string_scores: [usize; 2],
    pub compression_scores: [usize; 2]
}

pub fn process(results: &bench::Results) -> Scores {
    let mut processed_scores = Scores {
        processor_scores: [0, 0],

        reverse_string_scores: [0, 0],
        compression_scores: [0, 0]
    };

    // Reverse string test

    // Single-core
    for s in &results.reverse_string[0] {
        processed_scores.processor_scores[0] += s.score;
        processed_scores.reverse_string_scores[0] += s.score;
    }

    // Multi-core
    for s in &results.reverse_string[1] {
        processed_scores.processor_scores[1] += s.score;
        processed_scores.reverse_string_scores[1] += s.score;
    }

    // Compression test

    // Single-core
    for s in &results.compression[0] {
        processed_scores.processor_scores[0] += s.score;
        processed_scores.compression_scores[0] += s.score;
    }

    // Multi-core
    for s in &results.compression[1] {
        processed_scores.processor_scores[1] += s.score;
        processed_scores.compression_scores[1] += s.score;
    }

    processed_scores.processor_scores[0] = processed_scores.processor_scores[0] / results.total_tests;
    processed_scores.processor_scores[1] = processed_scores.processor_scores[1] / results.total_tests;

    return processed_scores;
}
