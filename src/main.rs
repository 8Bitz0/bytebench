// warnings: "mass-change the level for lints which produce warnings"
#![deny(warnings)]
// dead_code: "detect unused, unexported items"
#![allow(dead_code)]
// unreachable_code: "detects unreachable code paths"
#![allow(unreachable_code)]

use clap::{Parser, Subcommand};

use export::DataExportFormat;
use log::{debug, info, error};

pub mod bench;
pub mod config;
pub mod export;
pub mod probe;
pub mod scores;
pub mod system;
pub mod tests;
pub mod ui;
pub mod view;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    command: Commands
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Start a benchmark
    Start { config_path: Option<String> },
    /// Config management
    #[command(subcommand)]
    Config(ConfigCommands)
}

#[derive(Subcommand, Debug)]
enum ConfigCommands {
    /// Generate a config
    Generate { config_path: String }
}

fn main() {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"));

    // Define the arguments after parsing command-line arguments
    let args = Args::parse();

    debug!("Recieved user command \"{:?}\"", args.command);

    match args.command {
        Commands::Start { config_path } => {
            let config = match &config_path {
                None => {
                    info!("No configuration specified, using defaults.");
                    config::get_default_config()
                },
                Some(o) => {
                    match config::read_config(&std::path::PathBuf::from(&o)) {
                        Ok(o) => o,
                        Err(e) => {
                            error!("Failed to read configuration with error: \"{}\"", e);

                            std::process::exit(5);
                        }
                    }
                }
            };

            info!("Gathering system information...");

            let sys_info = match system::get_info() {
                Ok(o) => {
                    debug!("System information collected");
                    o
                }
                Err(_) => {
                    error!("Failed to get system information!");
                    std::process::exit(2);
                }
            };

            info!("Starting a benchmark...");

            let tests: Vec<String>;

            match config.tests {
                Some(o) => {
                    tests = o;
                }
                None => {
                    tests = config::get_default_config().tests.unwrap();
                }
            }

            let threads = match bench::run(sys_info.logical_cores, tests) {
                Ok(o) => {
                    debug!("Benchmark appears to have finished correctly");
                    o
                }
                Err(e) => {
                    error!("Benchmark failed with error: \"{}\", results discarded", e);
                    std::process::exit(1);
                }
            };

            info!("Calculating score...");

            let scores = scores::process(&threads);

            let results = export::DataExportFormat {
                scores: scores,
                threads: threads,
                sys_info: sys_info
            };

            match config.export_methods {
                Some(export_methods) => {
                    info!("Exporting...");
                    match export_process(export_methods.clone(), results.clone()) {
                        Ok(_) => {}
                        Err(e) => {
                            error!("Exporting failed with error: \"{}\"", e);
                            std::process::exit(4);
                        }
                    }
                }
                None => {
                    match export_process(config::get_default_config().export_methods.unwrap(), results.clone()) {
                        Ok(_) => {}
                        Err(e) => {
                            error!("Exporting failed with error: \"{}\"", e);
                            std::process::exit(4);
                        }
                    }
                }
            }

            view::print_info(results);
        }

        Commands::Config(config_command) => {
            match config_command {
                ConfigCommands::Generate { config_path } => {
                    match config::generate_config(&std::path::PathBuf::from(&config_path)) {
                        Ok(_) => {
                            info!("Created config at \"{}\".", &config_path);
                        }
                        Err(e) => {
                            error!("Failed to generate config with error: \"{}\"", e);

                            std::process::exit(6);
                        }
                    }
                }
            }
        }
    }
}

fn export_process(export_methods: Vec<config::ExportMethod>, results: DataExportFormat) -> Result<(), Box<dyn std::error::Error>> {
    let cwd = match std::env::current_dir() {
        Ok(o) => o,
        Err(e) => {
            error!("Failed to get current directory with error: \"{}\"", e);

            std::process::exit(3);
        }
    };

    for method in export_methods {
        let mut file_name_template = results.sys_info.hostname.clone();
        file_name_template.push_str("-report");

        if method.id == "json" {
            let mut json_path: std::path::PathBuf;

            match method.path {
                Some(path) => {
                    json_path = std::path::PathBuf::from(path)
                }
                None => {
                    json_path = cwd.clone();

                    let mut json_file_name = file_name_template.clone();
                    json_file_name.push_str(".json");
                    json_path.push(json_file_name);
                }
            }

            export::json::export(&results, &json_path)?;
            info!("Results exported as JSON!");
        }

        else if method.id == "html_simple" {
            let mut html_path: std::path::PathBuf;

            match method.path {
                Some(path) => {
                    html_path = std::path::PathBuf::from(path)
                }
                None => {
                    html_path = cwd.clone();

                    let mut html_file_name = file_name_template.clone();
                    html_file_name.push_str("-simple.html");
                    html_path.push(html_file_name);
                }
            }

            export::html_simple::export(&results, &html_path)?;
            info!("Results exported as HTML!");
        }

        else {
            error!("Unknown export method: \"{}\", skipping.", method.id);
        }
    }

    return Ok(());
}
