pub fn progress_prefix(min: usize, max: usize) -> String {
    let mut prefix = String::from("[");

    prefix.push_str(min.to_string().as_str());
    prefix.push_str("/");
    prefix.push_str(max.to_string().as_str());

    prefix.push_str("]");

    return prefix;
}
