use serde::Serialize;

use sysinfo::{System, SystemExt, CpuExt};

#[derive(Clone, Debug, Serialize)]
pub struct SystemInfo {
    pub hostname: String,
    pub os: String,
    pub os_distro: String,
    pub os_version: String,
    pub os_kernel_ver: String,

    pub physical_cores: usize,
    pub logical_cores: usize,
    pub cpu_brands: Vec<String>,
    pub cpu_vendors: Vec<String>,

    pub total_memory: u64,
}

pub const X86_FEATURES: [&str; 49] = [
    "aes",
    "pclmulqdq",
    "rdrand",
    "rdseed",
    "tsc",
    "mmx",
    "sse",
    "sse3",
    "ssse3",
    "sse4.1",
    "sse4.2",
    "sse4a",
    "sha",
    "avx",
    "avx2",
    "avx512f",
    "avx512cd",
    "avx512er",
    "avx512pf",
    "avx512bw",
    "avx512dq",
    "avx512vl",
    "avx512ifma",
    "avx512vbmi",
    "avx512vpopcntdq",
    "avx512vbmi2",
    "avx512gfni",
    "avx512vaes",
    "avx512vpclmulqdq",
    "avx512vnni",
    "avx512bitalg",
    "avx512bf16",
    "avx512vp2intersect",
    "f16c",
    "fma",
    "bmi1",
    "bmi2",
    "lzcnt",
    "tbm",
    "popcnt",
    "fxsr",
    "xsave",
    "xsaveopt",
    "xsaves",
    "xsavec",
    "cmpxchg16b",
    "adx",
    "rtm",
    "abm"
];

fn check_opt_str(string: Option<String>) -> String {
    match string {
        Some(o) => return o,
        None => return "Unknown".to_string()
    }
}

fn check_opt_int(int: Option<usize>) -> usize {
    match int {
        Some(o) => return o,
        None => return 0
    }
}

pub fn get_info() -> Result<SystemInfo, Box<dyn std::error::Error>> {
    let sys = System::new_all();

    let mut cpu_brands: Vec<String> = vec![];
    let mut cpu_vendors: Vec<String> = vec![];

    for cpu in sys.cpus() {
        if ! cpu_brands.contains(&cpu.brand().to_string()) && cpu.brand() != "" {
            cpu_brands.push(cpu.brand().to_string())
        }

        if ! cpu_vendors.contains(&cpu.vendor_id().to_string()) && cpu.vendor_id() != "" {
            cpu_vendors.push(cpu.vendor_id().to_string())
        }
    }

    let system_info = SystemInfo {
        hostname: check_opt_str(sys.host_name()),
        os: check_opt_str(sys.name()),
        os_version: check_opt_str(sys.long_os_version()),
        os_distro: sys.distribution_id(),
        os_kernel_ver: check_opt_str(sys.kernel_version()),

        physical_cores: check_opt_int(sys.physical_core_count()),
        logical_cores: sys.cpus().len().into(),
        cpu_brands: cpu_brands,
        cpu_vendors: cpu_vendors,

        total_memory: sys.total_memory()
    };

    return Ok(system_info);
}
