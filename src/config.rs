use std::{path::PathBuf, vec, io::Write};

use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize)]
pub struct BenchConfig {
    // Fancy name, so use capitalization
    pub name: String,
    // Config version
    pub version: u32,
    // Tests to perform
    pub tests: Option<Vec<String>>,
    // Export methods
    pub export_methods: Option<Vec<ExportMethod>>
}

#[derive(Clone, Deserialize, Serialize)]
pub struct ExportMethod {
    // Export method ID
    pub id: String,
    // Path to export to
    pub path: Option<String>
}

pub fn get_default_config() -> BenchConfig {
    return BenchConfig {
        name: String::from("Default"),
        version: 1,
        tests: Some(
            vec![
                String::from("reverse_string"),
                String::from("compression")
            ]
        ),
        export_methods: Some(
            vec![
                ExportMethod {
                    id: String::from("json"),
                    path: None
                },
                ExportMethod {
                    id: String::from("html_simple"),
                    path: None
                }
            ]
        )
    };
}

pub fn read_config(path: &PathBuf) -> Result<BenchConfig, Box<dyn std::error::Error>> {
    let config_string = std::fs::read_to_string(path)?;
    let parsed_config: BenchConfig = serde_json::from_str(&config_string)?;

    // Check for duplicate tests
    match &parsed_config.tests {
        Some(tests) => {
            let mut used_tests: Vec<String> = vec![];

            for test in tests {
                if used_tests.contains(test) {
                    let mut error = String::from("Duplicate test \"");
                    error.push_str(test);
                    error.push_str("\" found");

                    return Err(error.into())
                }

                used_tests.push(test.to_string());
            }
        }
        None => {}
    }

    return Ok(parsed_config);
}

pub fn generate_config(path: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    if path.is_file() || path.is_dir() {
        let error = String::from("File or directory already exists");

        return Err(error.into());
    }

    let mut config = get_default_config();
    config.name = String::from("New Config");

    let writable_config = serde_json::to_string_pretty(&config)?;

    let mut config_file = std::fs::File::create(path)?;
    config_file.write_all(writable_config.as_bytes())?;

    return Ok(());
}
