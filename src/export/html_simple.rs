use log::{info};

use std::io::Write;
use std::path::PathBuf;

use super::{DataExportFormat, printable_string_list};

pub fn export(results: &DataExportFormat, path: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    let mut doc: String = String::from("<!DOCTYPE html>");

    doc.push_str("<html>");

    // Neat CSS - https://github.com/codazoda/neatcss
    doc.push_str("<style>:root {color-scheme: dark light;--light: #fff;--dark: #404040;}* {box-sizing: border-box;color: var(--dark);}html {border-style: solid;border-width: 5px 0 0 0;border-color: var(--dark);}html, button, input {font-family: sans-serif;}input {border: 1px solid silver;border-radius: 5px;padding: 5px;}body {background-color: var(--light);margin: 0;max-width: 800px;padding: 0 20px 20px 20px;line-height: 1.4em;margin-left: auto;margin-right: auto;}p {margin-bottom: 10px;}img {width: 100%;}pre {overflow: auto;}button, .button, input[type=submit] {display: inline-block;background-color: var(--dark);color: var(--light);font-size: 1em;text-align: center;padding: 8px;margin-right: 5px;border-radius: 5px;text-decoration: none;border: none;line-height: normal;cursor: pointer;}button:last-child, .button:last-child {margin-right: 0;}.center {display: block;margin-left: auto;margin-right: auto;text-align: center;}.bordered {border: 3px solid;}.home {display: inline-block;background-color: var(--dark);color: var(--light);margin-top: 20px;padding: 5px 10px 5px 10px;text-decoration: none;font-weight: bold;}@media only screen and (min-width: 600px) {ol {column-count: 2;}.row {display: flex;flex-direction: row;padding: 0;width: 100%;}.row .column {display: block;flex: 1 1 auto;max-width: 100%;width: 100%;}.row .column:not(:last-child) {margin-right: 10px;}}@media (prefers-color-scheme: dark) {* {color: var(--light);}html {border-color: var(--light);}body {background-color: var(--dark);}button, .button, .home, input {background-color: var(--light);color: var(--dark);}}</style>");

    doc.push_str("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");

    doc.push_str("<head>");
    doc.push_str("<h1>Report - Bytebench</h1>");
    doc.push_str("</head><body>");

    doc.push_str("<h2>Single-Core: ");
    doc.push_str(&results.scores.processor_scores[0].to_string());
    doc.push_str("  Multi-Core: ");
    doc.push_str(&results.scores.processor_scores[1].to_string());
    doc.push_str("</h2>");

    doc.push_str("<h2>OS</h2>");

    doc.push_str("<ul>");

    doc.push_str("<li>Hostname: ");
    doc.push_str(&results.sys_info.hostname);
    doc.push_str("</li>");

    doc.push_str("<li>OS: ");
    doc.push_str(&results.sys_info.os);
    doc.push_str("</li>");

    doc.push_str("<li>OS Version: ");
    doc.push_str(&results.sys_info.os_version);
    doc.push_str("</li>");

    doc.push_str("<li>OS Distro: ");
    doc.push_str(&results.sys_info.os_distro);
    doc.push_str("</li>");

    doc.push_str("<li>Kernel Version: ");
    doc.push_str(&results.sys_info.os_kernel_ver);
    doc.push_str("</li>");

    doc.push_str("</ul>");

    doc.push_str("<li>Physical Cores: ");
    doc.push_str(&results.sys_info.physical_cores.to_string());
    doc.push_str("</li>");

    doc.push_str("<li>Logical Cores: ");
    doc.push_str(&results.sys_info.logical_cores.to_string());
    if &results.sys_info.logical_cores.into() == &results.sys_info.physical_cores * 2 {
      doc.push_str(" (SMT Detected)");
    }
    doc.push_str("</li>");

    doc.push_str("<li>CPU Name(s): ");
    doc.push_str(&printable_string_list(&results.sys_info.cpu_brands));
    doc.push_str("</li>");

    doc.push_str("<li>CPU Vendor(s): ");
    doc.push_str(&printable_string_list(&results.sys_info.cpu_vendors));
    doc.push_str("</li>");

    doc.push_str("</ul>");


    doc.push_str("<h2>Memory</h2>");

    doc.push_str("<ul>");

    doc.push_str("<li>Total Memory: ");
    doc.push_str(&(&results.sys_info.total_memory / 1000000).to_string());
    doc.push_str(" MB");
    doc.push_str("</li>");

    doc.push_str("</ul>");

    doc.push_str("</body></html>");

    match std::fs::File::create(path) {
      Ok(mut f) => {
         info!("Exporting results as HTML to {}", path.to_string_lossy());

         f.write_all(doc.as_bytes())?;
      }
      Err(e) => {
         return Err(Box::new(e));
      }
    }

    return Ok(());
}
