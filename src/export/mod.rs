// warnings: "mass-change the level for lints which produce warnings"
#![deny(warnings)]
// dead_code: "detect unused, unexported items"
#![allow(dead_code)]
// unreachable_code: "detects unreachable code paths"
#![allow(unreachable_code)]

use serde::Serialize;

use crate::bench;
use crate::scores;
use crate::system;

#[derive(Clone, Debug, Serialize)]
pub struct DataExportFormat {
   pub scores: scores::Scores,
   pub threads: bench::Results,
   pub sys_info: system::SystemInfo
}

pub fn printable_string_list(list: &Vec<String>) -> String {
    let mut printable_list = String::new();

    let mut count: usize = 1;

    for s in list {
        printable_list.push_str(&s);

        if count < list.len() {
            printable_list.push_str(", ");
        }

        count += 1;
    }

    return printable_list;
}

pub mod html_simple;
pub mod json;
