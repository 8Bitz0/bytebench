use log::{info};

use std::io::Write;
use std::path::PathBuf;

use super::DataExportFormat;

pub fn export(results: &DataExportFormat, path: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {
    match std::fs::File::create(path) {
        Ok(mut f) => {
            info!("Exporting results as JSON to {}", path.to_string_lossy());

            match serde_json::to_string_pretty(&results) {
                Ok(o) => {
                    f.write_all(o.as_bytes())?;
                }
                Err(e) => {
                    return Err(Box::new(e));
                }
            }
        }
        Err(e) => {
            return Err(Box::new(e));
        }
    }

    return Ok(());
}
