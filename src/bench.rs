use log::info;

use serde::Serialize;

use crate::probe;
use crate::tests;
use crate::ui;

#[derive(Clone, Debug, Serialize)]
pub struct Results {
    pub total_tests: usize,

    pub reverse_string: [Vec<probe::ThreadInfo>; 2],
    pub compression: [Vec<probe::ThreadInfo>; 2]
}

pub fn run(cores: usize, tests: Vec<String>) -> Result<Results, Box<dyn std::error::Error>> {
    let mut results = Results {
        total_tests: tests.len(),

        reverse_string: [vec![], vec![]],
        compression: [vec![], vec![]]
    };

    let test_info_list = tests::get_tests();
    let tests_length = tests.len();

    let mut test_num: usize = 0;

    for test in tests {
        test_num += 1;

        if test == "reverse_string" {
            for test_info in &test_info_list {
                if test_info.id == test {
                    info!("{} - {}", ui::progress_prefix(test_num, tests_length), test_info.name);
                }
            }

            // Reverse string test
            // Single-core
            match tests::reverse::start(1) {
                Ok(o) => {
                    results.reverse_string[0] = o;
                }
                Err(e) => {
                    return Err(e);
                }
            }

            // Multi-core
            match tests::reverse::start(cores) {
                Ok(o) => {
                    results.reverse_string[1] = o;
                }
                Err(e) => {
                    return Err(e);
                }
            }
        }

        else if test == "compression" {
            for test_info in &test_info_list {
                if test_info.id == test {
                    info!("{} - {}", ui::progress_prefix(test_num, tests_length), test_info.name);
                }
            }

            // Compression test
            // Single-core
            match tests::compress::start(1) {
                Ok(o) => {
                    results.compression[0] = o;
                }
                Err(e) => {
                    return Err(e);
                }
            }

            // Multi-core
            match tests::reverse::start(cores) {
                Ok(o) => {
                    results.compression[1] = o;
                }
                Err(e) => {
                    return Err(e);
                }
            }
        }

        else {
            let mut error = String::from("Invalid test \"");
            error.push_str(&test);
            error.push_str("\"");
            
            return Err(error.into());
        }
    }

    return Ok(results);
}
