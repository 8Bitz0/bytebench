build:
	cargo build --profile release

prep:
	cargo install cargo-deb
	cargo install cargo-generate-rpm
	cargo install cargo-arch

.init: build
	-mkdir ./dist/
	echo "If you recieve a 'no such subcommand' error, try 'make prep'."

.build-deb:
	cargo deb -o ./dist/

.build-rpm:
	cargo generate-rpm -o ./dist/

.build-arch:
	cargo arch
	mv *.pkg.tar.zst dist/
	mv PKGBUILD dist/

deb: .init .build-deb

rpm: .init .build-rpm

arch: .init .build-arch

release: .init .build-deb .build-rpm
	echo "Packaging for Arch Linux needs to be done on Arch Linux. To run it, use 'make arch'."
